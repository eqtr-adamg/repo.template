## Synopsis

This is an empty repo with the minimal required files for a quicker setup. It includes standard .gitignore and gitattributes files, and NuGet is configured to work with the Eqtr packages. If forking this repo, replace this Synopsis with a project description, who the client is etc

## Installation



Brief explanation of setup steps required to run the project.

## Tests

Describe and show how to run the tests.

## Contributors

Links to JIRA, confluence, wiki, etc.